# ##### BEGIN GPL LICENSE BLOCK #####
#
#  This program is free software; you can redistribute it and/or
#  modify it under the terms of the GNU General Public License
#  as published by the Free Software Foundation; either version 2
#  of the License, or (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software Foundation,
#  Inc., 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301, USA.
#
# ##### END GPL LICENSE BLOCK #####

"""
This script is to test an optimal method of doing random
point intersection lookups for a polygon.


Its quite simple:
- sort polygon on the Y axis.
- for each Y axis store a sorted list of intersecting edges.

And for intersection lookups:
- find the Y interval of intersection (using b-tree).
- count the number of intersections (hits)
- return True (for an odd number of hits)
"""

FLT_MAX = __import__("sys").float_info.max


class Range:
    __slots__ = (
        "min",
        "max",
        )

    def __init__(self, min=None, max=None):
        self.min = min
        self.max = max

    def copy(self):
        return Range(self.min, self.max)

    def expand(self, other):
        self.min = min(self.min, other.min)
        self.max = max(self.max, other.max)


class XEdge:
    # y values are implicit
    __slots__ = (
        "xrange",  # x of this axis, pair (curr, next)
        )

    def __init__(self, xrange):
        self.xrange = xrange


class YPoint:
    __slots__ = (
        "index",
        "xedges",
        # quick lookup only
        "xedges_range",
        )

    def __init__(self, index):
        self.index = index
        self.xedges = []
        self.xedges_range = Range()


class PolyScan:
    __slots__ = (
        "ypoints",
        "polygon",
        )


    def __init__(self, polygon):
        polygon_sorted = list(range(len(polygon)))
        polygon_sorted.sort(key=lambda i: polygon[i][1])
        self.ypoints = [YPoint(-1) for i in range(len(polygon))]

        edge_pool = []
        i_ypoint = 0

        i_poly_last = len(polygon_sorted) - 1
        while polygon[polygon_sorted[i_poly_last]][1] == polygon[polygon_sorted[-1]][1]:
            i_poly_last -= 1

        i_poly_curr = 0
        while i_poly_curr <= i_poly_last:

            # enter loop at least once, keep looping while on this Y value
            i_poly_next = i_poly_curr
            while polygon[polygon_sorted[i_poly_curr]][1] == polygon[polygon_sorted[i_poly_next]][1]:
                # edges are implicit
                i_curr = polygon_sorted[i_poly_next]
                i_others = ((i_curr + 1) % len(polygon),
                            (i_curr - 1) if i_curr else (len(polygon) - 1))

                # populate edges
                for j in range(2):
                    if polygon[i_others[j]][1] > polygon[i_curr][1]:
                        edge_pool.append((i_curr, i_others[j]))
                del i_curr, i_others

                i_poly_next += 1
                if i_poly_next == len(polygon_sorted):
                    break
            assert(i_poly_curr != i_poly_next)
            # use ypoints
            p_curr = self.ypoints[i_ypoint]
            p_next = self.ypoints[i_ypoint + 1]
            p_curr.index = polygon_sorted[i_poly_curr]
            p_next.index = polygon_sorted[i_poly_next]
            i_ypoint += 1

            y_curr = polygon[p_curr.index][1]
            y_next = polygon[p_next.index][1]
            assert(y_curr < y_next)

            p_curr.xedges_range.min = +FLT_MAX
            p_curr.xedges_range.max = -FLT_MAX

            # add to xedges
            j = len(edge_pool)
            while j:
                j -= 1
                ep = edge_pool[j]

                assert(polygon[ep[0]][1] <= y_curr)
                assert(polygon[ep[1]][1] >= y_next)

                v_curr = polygon[ep[0]]  # low
                v_next = polygon[ep[1]]  # high
                y_range = v_next[1] - v_curr[1]
                assert(y_range >= 0.0)

                # calc X ranges
                fac_next = (y_curr - v_curr[1]) / y_range
                fac_curr = 1.0 - fac_next
                x_curr = (v_curr[0] * fac_curr) + (v_next[0] * fac_next)

                fac_next = (y_next - v_curr[1]) / y_range
                fac_curr = 1.0 - fac_next
                x_next = (v_curr[0] * fac_curr) + (v_next[0] * fac_next)

                if x_curr < x_next:
                    p_curr.xedges_range.min = min(p_curr.xedges_range.min, x_curr)
                    p_curr.xedges_range.max = max(p_curr.xedges_range.max, x_next)
                else:
                    p_curr.xedges_range.min = min(p_curr.xedges_range.min, x_next)
                    p_curr.xedges_range.max = max(p_curr.xedges_range.max, x_curr)

                p_curr.xedges.append(XEdge((x_curr, x_next)))

                assert(polygon[ep[0]][1] <= y_curr)
                assert(polygon[ep[1]][1] >= y_next)
                if polygon[ep[1]][1] == y_next:
                    if j + 1 != len(edge_pool):
                        edge_pool[j] = edge_pool[-1]
                    del edge_pool[-1]

            p_curr.xedges.sort(key=lambda xe: min(xe.xrange))
            p_curr.xedges_range.min, p_curr.xedges_range.max = min(p_curr.xedges[0].xrange), max(p_curr.xedges[-1].xrange)

            i_poly_curr = i_poly_next

        # terminating point
        p = self.ypoints[i_ypoint]
        p.xedges_range.min = p.xedges_range.max = polygon[p.index][0]

        # remove any invalid
        del self.ypoints[i_ypoint + 1:]

        self.polygon = polygon

    def ps_bsearch_ypoints(self, y):
        ypoints = self.ypoints
        polygon = self.polygon

        def INDEX_Y(i):
            return polygon[ypoints[i].index][1]

        min_ = 0
        max_ = len(ypoints)
        while (min_ < max_):
            m = (min_  +  max_) // 2
            if (INDEX_Y(m) < y):
                min_ = m + 1
            else:
                max_ = m
        return max_ - (INDEX_Y(max_) != y)

    def isect(self, pt):

        # first check Y range
        if pt[1] < self.polygon[self.ypoints[0].index][1]:
            return False
        if pt[1] >= self.polygon[self.ypoints[-1].index][1]:
            return False
        # TODO, boundbox X range

        i_curr = self.ps_bsearch_ypoints(pt[1])
        if i_curr == -1:
            return False

        # first check the obvious case
        i_next = i_curr + 1
        p = self.ypoints[i_curr]
        if pt[0] < p.xedges_range.min or pt[0] > p.xedges_range.max:
            return False

        # get the right interval
        y_curr = self.polygon[self.ypoints[i_curr].index][1]
        y_next = self.polygon[self.ypoints[i_next].index][1]

        assert(pt[1] >= y_curr)
        assert(pt[1] < y_next)

        fac_next = (pt[1] - y_curr) / (y_next - y_curr)
        fac_prev = 1.0 - fac_next
        hits = False

        for xe in p.xedges:
            x_min, x_max = xe.xrange
            if x_min > x_max:
                x_min, x_max = x_max, x_min

            if x_min > pt[0]:
                break

            if ((pt[0] >= x_max) or
                (pt[0] >= (xe.xrange[0] * fac_prev) + (fac_next * xe.xrange[1]))):

                hits = not hits

        return hits


def isect_slow(poly, pt):
    isect = False
    nr = len(polygon)
    i = 0
    j = nr - 1
    while i < nr:
        if (((poly[i][1] > pt[1]) != (poly[j][1] > pt[1])) and
             (pt[0] < (poly[j][0] - poly[i][0]) * (pt[1] - poly[i][1]) / (poly[j][1] - poly[i][1]) + poly[i][0])):
            isect = not isect
        j = i
        i += 1
    return isect


# ----------------------------------------------------------------------------
# Test Case


polygon = (
 (0.22387051582336426, 0.3689805567264557),
 (0.3755171298980713, 0.4916589856147766),
 (0.034545183181762695, 0.6896727681159973),
 (0.41368624567985535, 0.9406747817993164),
 (0.6513701677322388, 0.717609167098999),
 (0.890160083770752, 0.8751264810562134),
 (0.8516140580177307, 0.6821978092193604),
 (0.9303067922592163, 0.7096627950668335),
 (0.8168893456459045, 0.6151142716407776),
 (0.9104523658752441, 0.5978955626487732),
 (0.8034576773643494, 0.5190416574478149),
 (0.3425253629684448, 0.7719507813453674),
 (0.5419802665710449, 0.4706556499004364),
 (0.38203179836273193, 0.34183362126350403),
 (0.8293724060058594, 0.3781990110874176),
 (0.8307129740715027, 0.3296704590320587),
 (0.7121834754943848, 0.29735806584358215),
 (0.6727588772773743, 0.10991284251213074),
 (0.5809520483016968, 0.09139183163642883),
 (0.5633570551872253, 0.23098312318325043),
 (0.39733877778053284, 0.05434975028038025),
 (0.30553197860717773, 0.03582867980003357),
 (0.28511667251586914, 0.11911669373512268),
 (0.4826560914516449, 0.2692433297634125),
 (0.24428588151931763, 0.28569257259368896),
 )

polygon = tuple([(float(a), float(b)) for a, b in (
(0.7, 0.37), (0.7, 0), (0.76, 0), (0.76, 0.4), (0.83, 0.4), (0.83, 0), (0.88, 0), (0.88, 0.4),
(0.94, 0.4), (0.94, 0), (1, 0), (1, 0.4), (0.03, 0.62), (0.03, 0.89), (0.59, 0.89), (0.03, 1),
(0, 1), (0, 0), (0.03, 0), (0.03, 0.37),
)])

print(polygon)


GRID = 100
DISPLAY = True
VALIDATE = True


# Slow function used to compare against
def test_reference():
    for y in range(GRID):
        y = GRID - y
        for x in range(GRID):
            pt = x / GRID, y / GRID
            isect = isect_slow(polygon, pt)

            if DISPLAY:
                print('*' if isect else ' ', end="")
        if DISPLAY:
            print()


# Fast function!
def test_fast():

    ps = PolyScan(polygon)

    import time
    t = time.time()
    for y in range(GRID):
        y = GRID - y
        for x in range(GRID):
            pt = x / GRID, y / GRID

            """
            import random
            pt = (pt[0] + ((random.random() - 0.5) / GRID),
                  pt[1] + ((random.random() - 0.5) / GRID))
            """

            isect = ps.isect(pt)

            if DISPLAY:
                print('*' if isect else ' ', end="")

            # just to be sure
            if VALIDATE:
                assert(isect == isect_slow(polygon, pt))

        if DISPLAY:
            print()

    print("done %.6f" % (time.time() - t))


if 0:
    test_reference()
else:
    test_fast()

