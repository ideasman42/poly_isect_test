# simple test for binary search on a sorted list

VERBOSE = 1

def bsearch_mid(arr, var_find, _ofs):
    steps = 0
    min_ = 0
    max_ = len(arr)

    if var_find >= arr[max_ - 1]:
        return max_ - 1, 0
    if var_find <= arr[0]:
        return 0, 0

    while (min_ < max_):
        m = (min_  +  max_) // 2
        if (arr[m] < var_find):
            min_ = m + 1
            if VERBOSE:
                print("  > ", min_ + _ofs)
        else:
            max_ = m
            if VERBOSE:
                print("  < ", m + _ofs)
        steps += 1
    return max_ - (arr[max_] != var_find), steps


def bsearch_min(arr, var_find, _ofs):
    steps = 0
    min_ = 0
    max_ = len(arr)

    if var_find >= arr[max_ - 1]:
        return max_ - 1, 0
    if var_find <= arr[0]:
        return 0, 0

    while (min_ < max_):
        m = (min_  +  max_) // 2
        if (arr[m] < var_find):
            min_ = m + 1
            if VERBOSE:
                print("  > ", min_ + _ofs)
        else:
            max_ = m
            #if (arr[min_] < var_find):
            #    min_ += 1
            if VERBOSE:
                print("  < ", m + _ofs)
        steps += 1
    return max_ - (arr[max_] != var_find), steps


def bsearch_min_dyn(arr, var_find, _ofs):
    steps = 0
    min_ = 0
    max_ = len(arr)

    if var_find >= arr[max_ - 1]:
        return max_ - 1, 0
    if var_find <= arr[0]:
        return 0, 0

    div = 2

    print("SEARCH", "min=", min_, "max=", max_, var_find)
    while (min_ < max_):
        print("  ", min_, max_, var_find)
        if div != 2 and (max_ - min_) // 2 <= div:
            div = 2
        m = min_ + ((max_ - min_) // div)
        print(m, min_, max_, "AAA", div)
        # assert(m not in (min_, max_))
        # print(m)
        if (arr[m] < var_find):
            min_ = m + 1
            div *= 2
            if VERBOSE:
                print("  > ", min_ + _ofs)
        else:
            max_ = m
            div = 2
            if VERBOSE:
                print("  < ", m + _ofs)
        steps += 1
    return max_ - (arr[max_] != var_find), steps


import random
TOT = 10000
arr = tuple([i for i in range(TOT) if random.randint(0, 4) == 0])

# print(arr)

#print(bsearch_min(arr, TOT / 2))
#print(bsearch_mid(arr, TOT / 2))


for fn in (bsearch_mid, bsearch_min, bsearch_min_dyn):
    print("Running", fn)
    tot_steps = 0
    i_prev = 0
    for var in range(0, TOT, TOT // 100):
        print("--- searching", var)

        args = (arr[i_prev :], var, i_prev)
        ret = fn(*args)

        # _i_prev_next, _steps = bsearch_mid(*args)
        # print("AAA")
        # print(i_prev_next, steps)
        # print(_i_prev_next, _steps)
        # print(ret, bsearch_mid(*args))

        assert(ret == bsearch_mid(*args))
        i_prev_next, steps = ret

        # assert((i_prev_next, steps) == bsearch_min(*args))
        # assert((i_prev_next, steps) == bsearch_min_dyn(*args))
        i_prev = i_prev_next + i_prev
        tot_steps += steps
    print(fn, tot_steps)


